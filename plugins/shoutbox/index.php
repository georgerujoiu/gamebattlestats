<?php

$panels = '<link rel="stylesheet" type="text/css" href="plugins/shoutbox/css/style.css" />';
$panels .= '<div class="panel-wrapper">';
		$panels .= '<div class="panel-title"><p>'.$row['panel_name'].'</p></div>';

	$panels .= '<div class="panel-content">';
		if(!empty($_SESSION['username'])){
		$panels .= '<form method="post">';
		$panels .= '<input type="hidden" name="shoutbox_name" id="shoutbox_name" value="'.$_SESSION['username'].'" />';
		if(isset($_GET['edit_shout']) && $_SESSION['check_rights'] == "admin"){
			$edit_shouts_res = $this->db->select('gamestats_shoutbox', '*', 'shoutbox_id = '.$_GET['edit_shout']);
			$edit_shout = $edit_shouts_res->fetch(PDO::FETCH_ASSOC);
			$panels .= '<textarea cols="19" rows="3" name="edit_shoutbox_message" class="textbox">'.$this->filter_words($edit_shout['shoutbox_message']).'</textarea>';
		} else {
		$panels .= "<textarea cols=\"19\" rows=\"3\" name=\"shoutbox_message\" id=\"shoutbox_message\" class=\"textbox\"></textarea>";
		}
		$panels .= "<input type=\"submit\" value=\"Shout\" id=\"shoutbox_button\" class=\"button\" /> <a href=\"\">:-)</a>";
		$panels .= "</form>";
		} else { $panels .= "<p style=\"text-align: center; padding-top: 10px; padding-bottom: 10px;\">You must be logged in to shout!</p>"; }
			$get_shouts = $this->db->select('gamestats_shoutbox', 'shoutbox_id, shoutbox_username, DATE_FORMAT(shoutbox_date, "%d-%m-%Y") as date, shoutbox_message', null, 'shoutbox_id DESC LIMIT 5');

			foreach($get_shouts as $row) {
				$fetch_user = $this->db->select('gamestats_members', 'member_id, member_avatar', array('member_username' => $row['shoutbox_username']));

				$panels .= '<div class="shoutbox_post">';
				$panels .= '<h4>';
				if($fetch_user[0]['member_avatar'] != NULL) {
					$panels .= "<img src=\"images/avatars/".$fetch_user[0]['member_avatar']."\" width=\"15px\" height=\"15px\" alt=\"\" />";
				} else {
					$panels .= "<img src=\"images/no_avatar.png\" width=\"15px\"	height=\"15px\" alt=\"\" />";
				}
				$panels .= '<a href="profile.php?view_profile='.$fetch_user[0]['member_id'].'">'.$row['shoutbox_username'].'</a></h4><p>['.$row['date'].']</p>';
				$panels .= '<p>'.$this->filter_words($this->smiley_parser($row['shoutbox_message'])).'</p>';
				if(isset($_SESSION['username']) && $_SESSION['check_rights'] == "admin") {
				$panels .= "<p style=\"font-size: 9px; margin-top: 4px;\"><a href=\"?edit_shout=".$row['shoutbox_id']."\" style=\"font-size: 9px\">Edit</a> / <a href=\"?delete_shout=".$row['shoutbox_id']."\" style=\"font-size: 9px\">Delete</a></p>";
					if(isset($_GET['delete_shout'])){
						$delete_shout = $this->check_input($_GET['delete_shout']);
						$this->db->delete('gamestats_shoutbox', 'shoutbox_id = '.$delete_shout);
						$this->redirect($_SERVER['PHP_SELF']);
					}
				}
				$panels .= "</div>";
		}
	$panels .= '<a class="more_shouts">More Shouts</a>';
	$panels .= "</div>";
	
	if(!empty($_POST['shoutbox_message']) && !empty($_POST['shoutbox_name'])) {
		$shoutbox_name = htmlspecialchars($_POST['shoutbox_name']);
		$shoutbox_message = htmlspecialchars($_POST['shoutbox_message']);
		$insert_shout = $this->db->insert("gamestats_shoutbox",  array('shoutbox_username' => $shoutbox_name, 'shoutbox_message' => $shoutbox_message, 'shoutbox_date' => 'NOW()'));
		$this->redirect($_SERVER['PHP_SELF']);
	} elseif( !empty($_POST['edit_shoutbox_message']) && !empty($_SESSION['username'])){	
		$message = $this -> check_input($_POST['edit_shoutbox_message']);
		if(!empty($message)){
		$res_update_shout = $this->db->update("gamestats_shoutbox", "shoutbox_message = ".$message, "shoutbox_id = ".$_GET['edit_shout']);
		$this->redirect($_SERVER['PHP_SELF']);
		}
	} elseif(isset($_GET['delete_shout'])){
		$delete_shout = $this->check_input($_GET['delete_shout']);
		$this->db->delete('gamestats_shoutbox', 'shoutbox_id = '.$delete_shout);
		$this->redirect($_SERVER['PHP_SELF']);
	}
$panels .= "</div>";

$panels .= '<script type="text/javascript" src="plugins/shoutbox/js/shoutbox.js"></script>';

?>
