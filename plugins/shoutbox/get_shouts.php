<?php
	include_once('../../classes/system.class.php');
	
	$system = new System();

	$limit = 5;
		
	if(isset($_GET['more_shouts'])) {
		$offset = $_GET['more_shouts'];
	} else {
		$offset = 0;
	}

	$get_shouts_res = $system->db->select('gamestats_shoutbox', '*', null, 'shoutbox_id DESC LIMIT '.$offset.', 5');
	
	
	foreach($get_shouts_res as $row) {
		$fetch_user = $system->db->select('gamestats_members', 'member_id, member_avatar', array('member_username'=>$row['shoutbox_username']));

		echo '<div class="shoutbox_post">';
		echo "<h4>";
		if($fetch_user[0]['member_avatar'] != NULL) {
			echo "<img src=\"images/avatars/".$fetch_user[0]['member_avatar']."\" width=\"15px\" height=\"15px\" alt=\"\" />";
		} else {
			echo "<img src=\"images/no_avatar.png\" width=\"15px\"	height=\"15px\" alt=\"\" />";
		}
		echo "<a href=\"profile.php?view_profile=".$fetch_user[0]['member_id']."\">".$row['shoutbox_username']."</a></h4><p class=\"shoutbox_date\">[".$row['shoutbox_date']."]</p>";
		echo "<p>".$row['shoutbox_message']."</p>";
		if(isset($_SESSION['username']) && $_SESSION['check_rights'] == "admin") {
			echo "<p class=\"shoutbox_admin\"><a href=\"?edit_shout=".$row['shoutbox_id']."\" style=\"font-size: 9px\">Edit</a> / <a href=\"?delete_shout=".$row['shoutbox_id']."\" style=\"font-size: 9px\">Delete</a></p>";
		}
		echo "</div>";
	}
?>