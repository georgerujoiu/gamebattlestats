<?php
require("classes/system.class.php");

$login = new System();
$login -> page_title = "Account Login";
if(!isset($_SESSION['username'])) {
	$login -> content = '<div class="login">
							<div class="social-buttons">
								<p>Login using social network</p>
								<button type="button" class="button facebook"><img src="images/icons/facebook.png" alt="" />Login with Facebook</button><button type="button" class="button twitter"><img src="images/icons/twitter.png" alt="" />Login with Twitter</button>
							</div>
							<div class="clear"></div>
							<form action="" method="post">
					 	 		<label for="username">Username</label><input type="text" name="username" id="username" class="textbox" />
						 		<label for="password">Password</label><input type="password" name="password" id="password" class="textbox" />
						 		<input type="hidden" name="member_ip" value="'.$_SERVER['REMOTE_ADDR'].'"/><br />
						 		<label for="remember" style="display: inline;">Keep me signed in</label><input type="checkbox" name="remember_me" />
						 		<input type="submit" class="button" value="Login" />
						 	</form>
						 	<a href="">Forgot your password?</a>
						 </div>';
} else {
	$login -> content = '<p class="error">You are already logged in! You will be redirected back to home page!<br />If you are not redirected in 3 seconds click <a href="index.php">here</a> to go to home page!</p>';
	$login -> content .= "<meta http-equiv='refresh' content='3;URL=index.php' />";
}
$login -> display_theme();
?>