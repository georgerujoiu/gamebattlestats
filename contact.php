<?php
include_once('classes/system.class.php');
include_once('classes/email.class.php');

$contact = new Email();
$contact -> page_title = "Contact";
$contact -> content = '<script type="text/javascript" src="js/hide_error.js"></script>
					   <form action="" method="post" class="contact">
					   <label for="email_email">*Your E-mail: </label>
					   <input type="text" name="email_email" id="email_email" size="40" class="textbox" />
					   <label for="email_subject">* Subject: </label>
					   <input type="text" name="email_subject" id="email_subject" size="40" class="textbox" />
					   <label for="email_message">* Message: </label>
					   <textarea cols="40" rows="8" name="email_message" class="textbox"></textarea>
					   <img src="captcha.php" id="email_image" />
					   <label for="captcha_image">* Type code: </label>
					   <input type="text" name="email_captcha" id="email_captcha" size="40" class="textbox" />
					   <input type="submit" value="Send E-mail" class="button" />
					   </form>';
if(isset($_POST['email_email']) && isset($_POST['email_subject']) && isset($_POST['email_message'])){
	$contact->send_email($_POST['email_email'], $_POST['email_subject'], $_POST['email_message']);
}
$contact -> display_theme();
?>
