<?php
	
require_once("classes/system.class.php");
require_once("classes/stats.class.php");
session_start();

$stats = new Stats();
$stats -> connect_db();
$stats -> title = "Stats";
$stats -> display_theme();
?>