<?php
	session_start();
	
	$captcha_string = "";
	for ($i=0; $i<5; $i++) {
		$captcha_string .= chr(rand(97,122));
	}
	$_SESSION['captcha_code'] = $captcha_string;
	$captcha_dir = "images/";

	$captcha_image = imagecreatetruecolor(90, 30);
	$captcha_black = imagecolorallocate($captcha_image, 0, 0, 0);
	$captcha_color = imagecolorallocate($captcha_image, 150, 0, 50);
	$captcha_white = imagecolorallocate($captcha_image, 240, 240, 240);

	imagefilledrectangle($captcha_image, 0, 0, 399, 99, $captcha_white);
	imagettftext($captcha_image, 20, 3, 8, 24, $captcha_color, $captcha_dir."arial.ttf", $_SESSION['captcha_code']);

	header("Content-type: image/png");
	imagepng($captcha_image);
?>
