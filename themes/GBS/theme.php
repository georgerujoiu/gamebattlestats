<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>{title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
<meta name="keywords" content="{keywords}" />
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="{theme_folder}/style.css" />
<script type="text/javascript" src="js/latest-jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8608563-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

$(document).ready(function() {
	{platforms_jquery}
});
</script>
<script type="text/javascript">
$(document).ready(function(){
$("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
$("#featured").hover(
function() {
$("#featured").tabs("rotate",0,true);
},
function() {
$("#featured").tabs("rotate",5000,true);
}
);
});
</script>
<script type="text/javascript" >
	function startTime() {
	var today = new Date();
	var d = today.getDate();
	var mo = today.getMonth()+1;
	var y = today.getFullYear();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
	
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById('thetime').innerHTML = d+"."+mo+"."+y+" / "+h+":"+m+":"+s;
		t = setTimeout('startTime()', 500);
	}
	
	function checkTime(i) {
		if(i<10) {
			i = "0"+i;
		}
		return i;
	}
	
	/*$(document).ready(function(){
		$('.loginbtn').click(function(e){
			e.preventDefault();
			$('.loginbar').animate({width:'toggle', height: 'toggle'},500, function(){
				$('.loginform').show('slow');					
			});
		});
	});*/
	
	$(document).ready(function(){
		if($('.left-side-panels').children().length < 1 && $('.right-side-panels').children().length < 1) {
			$('.center-panels-wrapper').css('width', '990px');
		} else if($('.left-side-panels').children().length < 1) {
			$('.center-panels-wrapper').css('width', '790px');
		} else if($('.right-side-panels').children().length < 1) {
			$('.center-panels-wrapper').css('width', '790px');
		}
		
		$(".platformsbtn").stop().hover(function(){
			$(".platforms").toggle("slow");
		});	
		
		{platforms_jquery}
		
			$(window).scroll(function() {
				if($(window).scrollTop() + $(window).height() > $(document).height() - 300) {	
  					$(".scrollArrow").fadeIn('slow');
  				} else {
  					$(".scrollArrow").fadeOut('slow');
  				}
			});
		
			$(".scrollArrow").click(function(){
				$('html, body').animate({scrollTop:0}, 'slow', function(){
					$(".scrollArrow").fadeOut('slow');
				});
			});
	});
</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "http://connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!--Platforms Navigation Bar-->
	<div class="topnav">
		<div class="container">	
			<p id="thetime">Welcome guest</p>
			<div class="loginbox">
				<div class="loginbar">
					{login}		
				</div>
				<a href="login.php"><div class="loginbtn">{login_btn}</div></a>
				<div class="registerbtn"><a href="register.php">Register</a></div>
			</div>
		</div>
	</div>
	<!--<div class="platforms-nav-wrapper">
		<div class="platforms-nav-secondary-wrap">
		<div class="platforms-nav-left"></div>
			<div class="platforms-nav-right">&nbsp;</div>
			<div class="platforms-nav-center">
				<ul class="platforms-nav">
					{platforms}
				</ul>
			</div>
		</div>
	</div>-->
	<!--End of Platforms Navigation Bar-->

	<!--Header-->
	<div class="header">
		<div class="logo"><a href="index.php"><img src="images/logo.png" alt="logo" /></a></div>
		<!--Status Panel-->
		<div class="status-panel">
			{last_online}
			<div style="display: none;">{login}</div>
		</div>
		<!--End of Status Panel-->
	</div>
	<!--End of Header-->
	
	<div class="subheader">
		<div class="container">
			<div class="subheader-left"></div>
			<div class="subheader-right"></div>
			<div class="subheader-center">
				{subheader}
				
				<div class="platformsbtn"><p>Platforms</p>
				<div class="platforms">
					{platforms}
				</div>
				</div>
			</div>
		</div>
	</div>

	<!--Content-->
	<div class="content-wrapper">

		<div class="sub-banner">
			<!--Slider-->
			{slider}
        <!-- First Content -->  
        
			<!--End of Slider-->
			<!--<div style="padding-top: 5px">
				<h4>Latest Match</h4>
				<div style="float: left; position: relative; left: 24px; top: 24px; width: 290px;">
				{latest_match_panel}
				</div>
			</div>-->
		</div>
		<!--End of SubBanner-->
		
		<div class="content-top"><p>{page_title}</p></div>
		<div class="content-center">
			<div class="left-side-panels">
				{left_panels}
			</div>
			<div class="center-panels-wrapper">
					{content}
			</div>
			<div class="right-side-panels">
				{right_panels}
			</div>
			<br style="clear: both" />
			</div>
	</div>
	<!--End of Content-->

	<!--Footer-->
	<div class="footer">
		<div class="footer-center">
			<p class="copyright">&copy; Copyright <a href="http://www.gbladder.com/" target="_blank">GameBattleStats</a> 2011. All rights reserved.  Version {current_version}</p>
			<div class="footer-icons">
				{footer_icons}
			</div>
			<div class="bottom-footer">
				<p class="footer-links"><a href="terms.php">Terms and Conditions</a>-<a href="faq.php">FAQ</a>-<a href="search.php">Adv. Search</a>-<a href="members.php">Members List</a>-<a href="teams.php">Teams List</a></p>
			</div>
		</div>
	</div>
	<!--End of footer-->
	
	<div class="scrollArrow"></div>

</body>
</html>