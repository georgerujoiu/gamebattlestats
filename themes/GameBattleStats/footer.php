	<!--Footer-->
	<div class="footer">
		<div class="footer-left">&nbsp;</div>
		<div class="footer-right">&nbsp;</div>
		<div class="footer-center">
			<div class="footer-block">
				<h3>News</h3>
				<ul>
					<li><a href="">Latest</a></li>
					<li><a href="">Featured</a></li>
					<li><a href="">Enhancements</a></li>
					<li><a href="">Bug Fixes</a></li>
				</ul>
			</div>
			<div class="footer-block">
				<h3>Forums</h3>
				<ul>
					<li><a href="">GameBattleStats</a></li>
					<li><a href="">Feature Requests</a></li>
					<li><a href="">PhpBB Support</a></li>
					<li><a href="">vBulletin Support</a></li>
					<li><a href="">Theme Requests</a></li>
					<li><a href="">Other</a></li>
				</ul>
			</div>
			<div class="footer-block">
				<h3>Support</h3>
				<ul>
					<li><a href="">Customer support</a></li>
					<li><a href="">FAQ</a></li>
				</ul>
			</div>
			<div class="footer-block">
				<h3>Documentation</h3>
				<ul>
					<li><a href="">Download & Install</a></li>
					<li><a href="">How to set up a theme</a></li>
					<li><a href="">Ladders</a></li>
					<li><a href="">Tournaments</a></li>
					<li><a href="">Other Features</a></li>
				</ul>
			</div>
			<div class="footer-block">
				<h3>Themes</h3>
				<ul>
					<li><a href="">Gallery</a></li>
					<li><a href="">Free</a></li>
					<li><a href="">Premium</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!--End of footer-->
	<div class="last">
		<p class="copyright">&copy; Copyright <a href="http://www.gbladder.com/" target="_blank">GameBattleStats</a> 2011. All rights reserved.  Version {current_version}</p>
		<div class="bottom-footer">
				<p class="footer-links"><a href="terms.php">Terms and Conditions</a>-<a href="faq.php">FAQ</a>-<a href="search.php">Adv. Search</a>-<a href="members.php">Members List</a>-<a href="teams.php">Teams List</a></p>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="scrollArrow"></div>
	<script type="text/javascript">
		$(function(){
			$('.side-facebook a').hover(function(){
				$(this).animate({opacity: 1, paddingLeft:"+=20px", backgroundColor:'#333333', borderTopRightRadius: 10, borderBottomRightRadius: 10});
			}, function(){
				$(this).animate({opacity: 0.5, paddingLeft:"-=20px", backgroundColor:'#111111',  borderTopRightRadius: 0, borderBottomRightRadius: 0});
			});
		});
	</script>
</body>
</html>