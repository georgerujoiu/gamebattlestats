	<div class="subheader">
		<div class="container">
			<div class="subheader-left"></div>
			<div class="subheader-right"></div>
			<div class="subheader-center">
				<ul>{subheader}</ul>
				
				<div class="search-form">
					<form action="search.php" method="post">
						<input type="text" name="search_value" id="search_value" placeholder="Search..." />
						<input type="submit" value="" />
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="platforms-nav-wrapper">
		<div class="platforms-nav-pattern">
			<ul class="platforms-nav">
				{platforms}
			</ul>
		</div>
	</div>

	<!--Content-->
	<div class="content-wrapper">
		<div class="content-center">
			<div class="center-panels-wrapper">
				<div class="slider-wrapper">
					{slider}
				</div>
				{content}
			</div>
			<div class="side-panels">
				<div class="left-side-panels">{left_panels}</div>
				<div class="right-side-panels">{right_panels}</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!--End of Content-->