<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
<meta name="keywords" content="{keywords}" />
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="themes/{theme_folder}/css/style.css" />
<link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/latest-jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8608563-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<script type="text/javascript" >
	function startTime() {
	var today = new Date();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
	
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById('thetime').innerHTML = h+":"+m+":"+s;
		t = setTimeout('startTime()', 500);
	}
	
	function checkTime(i) {
		if(i<10) {
			i = "0"+i;
		}
		return i;
	}
	
	$(document).ready(function(){
		if($('.left-side-panels').children().length < 1 && $('.right-side-panels').children().length < 1) {
			$('.center-panels-wrapper').css('width', '990px');
		} else if($('.left-side-panels').children().length < 1) {
			$('.center-panels-wrapper').css('width', '790px');
		} else if($('.right-side-panels').children().length < 1) {
			$('.center-panels-wrapper').css('width', '790px');
		}
		
		$(".platformsbtn").stop().hover(function(){
			$(".platforms").toggle("slow");
		});
		
		$('.user_nav').hover(function() {
			$('.slide_down_nav').show();
		}, function() {
			$('.slide_down_nav').hide();
		});	
		
		{platforms_jquery}
		
			$(window).scroll(function() {
				if($(window).scrollTop() + $(window).height() > $(document).height() - 300) {	
  					$(".scrollArrow").fadeIn('slow');
  				} else {
  					$(".scrollArrow").fadeOut('slow');
  				}
			});
		
			$(".scrollArrow").click(function(){
				$('html, body').animate({scrollTop:0}, 'slow', function(){
					$(".scrollArrow").fadeOut('slow');
				});
			});
	});
</script>
</head>
<body onload="startTime()">
<div class="side-facebook"><a href="https://www.facebook.com/pages/GameBattleStats-Gaming-Ladder-Script/255165264545942" target="_blank"></a></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "http://connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!--Platforms Navigation Bar-->
	<div class="topnav">
		<div class="topnav-left"></div>
		<div class="topnav-right"></div>
		<div class="topnav-center">	
			{user_nav}
			<div class="topnav-button">
				{top_nav}
				<p class="the_time">The time is: <span id="thetime"></span></p>
			</div>
		</div>
	</div>

	<!--Header-->
	<div class="header">
		<div class="logo"><a href="index.php"><img src="themes/{theme_folder}/images/logo.png" alt="logo" /></a></div>
		<div class="clear"></div>
	</div>
	<!--End of Header-->