<?php
  require_once('header.php');
  $query = $admin->db->select('gamestats_news LEFT JOIN gamestats_members ON gamestats_news.news_postby = gamestats_members.member_id', 
                              'news_id, news_title, news_content, news_postby, DATE_FORMAT(news_postdate,"%d-%m-%Y") as news_date, member_username',
                              null, 'news_id DESC');	
?>    
    <h3 class="pull-left">Posts</h3>
      <div class="buttons pull-right">
        <a class="btn btn-primary" href="add_post.php">Add Post</a>
        <button class="btn btn-danger" type="button">Delete Selected</button>&nbsp;
      </div>

      <table class="table sortable">
      <thead>
		    <tr><th><input type="checkbox" class="checkAll" title="Select All" /></th><th>Title</th><th>Summary</th><th>Date</th><th>Category</th><th>Author</th><th>Actions</th></tr>
      </thead>
      <tbody>
	       <?php
	  	      foreach($query as $row) {
			        echo '<tr><td><input type="checkbox" /></td><td>'.$row['news_title'].'</td><td>'.$row['news_content'].'</td><td>'.$row['news_date'].'</td><td>PC</td><td>'.$row['member_username'].'</td><td><a href=""><i class="icon-pencil" title="Edit"></i></a><a href="?delete_pid='.$row['news_id'].'""><i class="icon-trash" title="Delete"></i></a></td></tr>';
		        }
          ?>
        </tbody>
      </table>

<?php

  if(isset($_GET['delete_pid'])) {
    $delete_post_id = $admin->check_input($_GET['delete_pid']);
    $admin->db->delete('gamestats_news', 'news_id = '.$delete_post_id);
  }

?>

      <!--Pagination-->
      <div class="pagination pagination-small">
      	<ul>
          <li><a>Prev</a></li>
          <li><a>1</a></li>
          <li><a>2</a></li>
          <li><a>Next</a></li>
          <?php //echo $admin->paginate->display_pagination(20, 5); ?>
      	</ul>
      </div>

  <?php require_once('footer.php'); ?>