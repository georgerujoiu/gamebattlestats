<?php
include_once('../classes/system.class.php');
include_once('../classes/admin.class.php');
$admin = new Admin();
if(!isset($_SESSION['username']) && !isset($_SESSION['check_rights'])) {
  if($_SESSION['check_rights'] != 'admin') {
    header('Location: login.php');
  }
}
?>
<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>GameBattleStats Administration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="index_files/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="index_files/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/ico/favicon.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#"><?php echo $admin->getSettings('title'); ?></a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="<?php echo $admin->site_url('admin'); ?>">Home</a></li>
              <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Articles<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="add_post.php">New Post</a></li>
					<li><a href="posts.php">Posts</a></li>
					<li><a href="categories.php">Categories</a></li>
				</ul>
			  </li>
        <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Ladders<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="platforms.php">Platforms</a></li>
					<li><a href="">Games</a></li>
				</ul>
	 		 </li>
	 		 <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tournaments<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="">Platforms</a></li>
					<li><a href="">Games</a></li>
				</ul>
	 		 </li>
              <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Membership<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="membership.php">Members</a></li>
          <li><a href="membership.php">Add Member</a></li>
				</ul>
	 		 </li>
	 		  <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="settings.php">General Settings</a></li>
          <li><a href="panels.php">Panels</a></li>
        </ul>
       </li>
	 		 <li><a href="action.php?logout=yes">Log Out</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">