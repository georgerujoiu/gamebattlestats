<?php include_once('header.php'); ?>

<form method="post">
    <fieldset>
        <legend>Settings</legend>
        <label>Site Title</label>
        <input type="text" name="site_title" value="<?php echo $admin->getSettings('title'); ?>">
        <label>E-mail</label>
        <input type="text" name="site_email" value="<?php echo $admin->getSettings('email'); ?>">
        <label>Description</label>
        <textarea class="input-xxlarge" name="site_description" placeholder="Type description here..." rows="6"></textarea>
        <label>Theme</label>
        <select class="input-xxlarge" name="site_theme">
        	<?php echo $admin->themeSelector(); ?>
        </select>
        <label>Language</label>
        <select class="input-xxlarge" name="site_theme">
        	<?php echo $admin->languageSelector(); ?>
        </select>
         <label class="checkbox">
			<input type="checkbox" name="site_maintenance"> Maintenance
		</label>
		<hr />
		 <label>Word Filter</label>
        <textarea class="input-xxlarge" name="site_word_filter" placeholder="Type words to filter here..." rows="6"></textarea>
        <br />
        <button type="submit" class="btn">Submit</button>
    </fieldset>
</form>

<?php include_once('footer.php'); ?>