<?php
  require_once('header.php');
  $query = $admin->db->select('gamestats_members', 'member_id, member_username, member_email, DATE_FORMAT(member_joindate, "%d-%m-%Y") as joindate, member_access', null, 'member_id DESC');	
?>    
    <h3 class="pull-left">Membership</h3>
      <button class="btn btn-danger pull-right" type="button">Delete Selected</button>

      <table class="table sortable">
      <thead>
		    <tr><th><input type="checkbox" class="checkAll" title="Select All" /></th><th>Title</th><th>Summary</th><th>Join Date</th><th>Access Level</th><th>Actions</th></tr>
      </thead>
      <tbody>
	       <?php
	  	      foreach($query as $row) {
			        echo '<tr><td><input type="checkbox" /></td><td>'.$row['member_username'].'</td><td>'.$row['member_email'].'</td><td>'.$row['joindate'].'</td><td>'.$row['member_access'].'</td><td><a href=""><i class="icon-pencil" title="Edit"></i></a><a href="?delete_pid='.$row['member_id'].'""><i class="icon-trash" title="Delete"></i></a></td></tr>';
		        }
          ?>
        </tbody>
      </table>

<?php

  if(isset($_GET['delete_pid'])) {
    $delete_post_id = $admin->check_input($_GET['delete_pid']);
    $admin->db->delete('gamestats_news', 'news_id = '.$delete_post_id);
  }

?>

      <!--Pagination-->
      <div class="pagination pagination-small">
      	<ul>
          <li><a>Prev</a></li>
          <li><a>1</a></li>
          <li><a>2</a></li>
          <li><a>Next</a></li>
          <?php //echo $admin->paginate->display_pagination(20, 5); ?>
      	</ul>
      </div>

  <?php require_once('footer.php'); ?>