<?php include_once('header.php'); ?>

<form method="post">
    <fieldset>
        <legend>Add Post</legend>
        <label>Title</label>
        <input type="text" name="post_title" placeholder="Type post title here…">
        <label>Description</label>
        <textarea class="input-xxlarge" name="post_content" placeholder="Type description here..." rows="6"></textarea>
        <!--<label>Post Date</label>
        <input type="text" name="post_title" class="datepicker" placeholder="dd-mm-yyyy">-->
        <br />
        <button type="submit" class="btn">Submit</button>
    </fieldset>
</form>

<?php include_once('footer.php'); ?>