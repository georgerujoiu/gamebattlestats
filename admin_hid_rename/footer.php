    </div>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="index_files/jquery.js"></script>
    <script src="index_files/bootstrap-transition.js"></script>
    <script src="index_files/bootstrap-alert.js"></script>
    <script src="index_files/bootstrap-modal.js"></script>
    <script src="index_files/bootstrap-dropdown.js"></script>
    <script src="index_files/bootstrap-scrollspy.js"></script>
    <script src="index_files/bootstrap-tab.js"></script>
    <script src="index_files/bootstrap-tooltip.js"></script>
    <script src="index_files/bootstrap-popover.js"></script>
    <script src="index_files/bootstrap-button.js"></script>
    <script src="index_files/bootstrap-collapse.js"></script>
    <script src="index_files/bootstrap-carousel.js"></script>
    <script src="index_files/bootstrap-typeahead.js"></script>
    <script src="index_files/jquery-ui-1.10.1.custom.min.js"></script>

    <!-- Ajax Request -->
    <script type="text/javascript">
        $(function(){
            $('form').submit(function(e){
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "action.php",
                    data:$('form').serialize()
                }).done(function(html) {
                    $(".container").empty().append(html);       
                });
            });

            //Preserve table rows width
            var fixHelper = function(e, ui) {
                ui.children().each(function(){
                    $(this).width($(this).width());
                });
                return ui;
            };

            //Sortable
            $( ".sortable tbody" ).sortable({helper: fixHelper}).disableSelection();

            //Load Datepicker
            $(function() {
                $( ".datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
            });
        });
    </script>

    <script type="text/javascript">
    $(function() {
      $('.checkAll').click(function(){
        $(this).parents('table').find(':checkbox').attr('checked', this.checked);
      });
    });
  </script>

</body></html>