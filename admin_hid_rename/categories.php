<?php
  require_once('header.php');
  $query = $admin->db->select('gamestats_news_cat', '*', null, 'news_cat_id DESC');	
?>    
    <h3 class="pull-left">Posts</h3>
      <div class="buttons pull-right">
        <a class="btn btn-primary" href="add_category.php">Add Category</a>
        <button class="btn btn-danger" id="delete_selected" type="button">Delete Selected</button>&nbsp;
      </div>

      <table class="table sortable">
      <thead>
		    <tr><th><input type="checkbox" class="checkAll" name="checkedItem" title="Select All" /></th><th>Title</th><th>Icon</th><th>Actions</th></tr>
      </thead>
      <tbody>
	       <?php
	  	      foreach($query as $row) {
			        echo '<tr><td><input type="checkbox" name="checkedItem" /></td><td>'.$row['news_cat_name'].'</td><td><img src="../images/news_cats/'.$row['news_cat_image'].'" alt="'.$row['news_cat_name'].'" /></td><td><a href=""><i class="icon-pencil" title="Edit"></i></a><a href="?delete_pid='.$row['news_cat_id'].'""><i class="icon-trash" title="Delete"></i></a></td></tr>';
		        }
          ?>
        </tbody>
      </table>

<?php

  if(isset($_GET['delete_pid'])) {
    $delete_post_id = $admin->check_input($_GET['delete_pid']);
    $admin->db->delete('gamestats_news', 'news_id = '.$delete_post_id);
  }

?>

      <!--Pagination-->
      <div class="pagination pagination-small">
      	<ul>
          <li><a>Prev</a></li>
          <li><a>1</a></li>
          <li><a>2</a></li>
          <li><a>Next</a></li>
          <?php //echo $admin->paginate->display_pagination(20, 5); ?>
      	</ul>
      </div>

  <?php require_once('footer.php'); ?>