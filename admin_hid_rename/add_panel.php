<?php include_once('header.php'); ?>

<form method="post">
    <fieldset>
        <legend>Add Panel</legend>
        <label>Title</label>
        <input type="text" name="panel_title" placeholder="Type panel title here…">
        <label>Panel</label>
        <select name="panel_url">
        	<?php $panels = $admin->scandir('../plugins');
        		foreach($panels as $panel) {
        			echo '<option>'.$panel.'</option>';
           		}
        	?>
        </select>
        <label>Panel Side</label>
        <select name="panel_side">
        	<option value="1">Left</option>
        	<option value="2">Center</option>
        	<option value="3">Right</option>
        </select>
        <br />
        <button type="submit" class="btn">Submit</button>
    </fieldset>
</form>

<?php include_once('footer.php'); ?>