<?php

include_once('../classes/system.class.php');
include_once('../classes/admin.class.php');
$admin = new Admin();

  //Posts
  if(isset($_POST['post_title']) && isset($_POST['post_content'])) {
    $post_title = $admin->check_input($_POST['post_title']);
    $post_content = $admin->check_input($_POST['post_content']);
    $admin->db->insert('gamestats_news', 'news_title, news_content, news_postdate, news_image, news_postby, news_cat_id', $post_title.', '.$post_content.', NOW(), NULL, 1, 1');
    $admin->redirect('posts.php');
  }

  if(isset($_GET['delete_pid'])) {
    $delete_post_id = $admin->check_input($_GET['delete_pid']);
    $admin->db->delete('gamestats_news', 'news_id = '.$delete_post_id);
    $admin->redirect('posts.php');
  }

  //News Categories
  if(isset($_POST['category_title'])) {
    $category_title = $admin->check_input($_POST['category_title']);
    $admin->db->insert('gamestats_news_cat', 'news_cat_name', $category_title);
    $admin->redirect('categories.php');
  }

  //Settings
  if(isset($_POST['site_title'])) {
    $site_title = $admin->check_input($_POST['site_title']);
    $admin->db->update('gamestats_settings', 'settings_value = '.$site_title, 'settings_name = "title"');
    $admin->redirect('settings.php');
  }

  //Panels
  if(isset($_POST['panel_title']) && isset($_POST['panel_side'])) {
    $panel_title = $admin->check_input($_POST['panel_title']);
    $panel_url = $admin->check_input($_POST['panel_url']);
    $panel_side = $admin->check_input($_POST['panel_side']);
    $admin->db->insert('gamestats_panels', 'panel_name, panel_url, panel_side', $panel_title.', '.$panel_url.', '.$panel_side);
    $admin->redirect('panels.php');
  }

  if(isset($_GET['logout'])) {
    $admin->logout();
  }

?>