<?php
require_once("classes/system.class.php");
require_once("classes/admin.class.php");

$admin = new Admin();
if(isset($_SESSION['check_rights']) && $_SESSION['check_rights'] == 'admin') {
$admin -> display_theme();
} elseif(isset($_COOKIE['check_rights']) && $_COOKIE['check_rights'] == 'admin') {
$admin -> display_theme();
} else {
	$admin->redirect('index.php');
}
?>
